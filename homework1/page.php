<!DOCTYPE html>
<html>
<head>
    <title>Student Information Form</title>
</head>
<body>
    <h2>Student Information Form</h2>
    <form action="" method="post">
        <label for="student_name">Student's Name:</label>
        <input type="text" name="student_name" required><br><br>

        <label for="student_surname">Student's Surname:</label>
        <input type="text" name="student_surname" required><br><br>

        <label for="course">Course:</label>
        <input type="text" name="course" required><br><br>

        <label for="semester">Semester:</label>
        <input type="text" name="semester" required><br><br>

        <label for="study_course">Study Course:</label>
        <input type="text" name="study_course" required><br><br>

        <label for="received_mark">Received Mark:</label>
        <input type="text" name="received_mark" required><br><br>

        <label for="lecturer_name">Lecturer's Name:</label>
        <input type="text" name="lecturer_name" required><br><br>

        <label for="lecturer_surname">Lecturer's Surname:</label>
        <input type="text" name="lecturer_surname" required><br><br>

        <input type="submit" name="submit" value="Submit">
    </form>

    <?php
    if (isset($_POST['submit'])) {
        $student_name = $_POST['student_name'];
        $student_surname = $_POST['student_surname'];
        $course = $_POST['course'];
        $semester = $_POST['semester'];
        $study_course = $_POST['study_course'];
        $received_mark = $_POST['received_mark'];
        $lecturer_name = $_POST['lecturer_name'];
        $lecturer_surname = $_POST['lecturer_surname'];

       
        $grade = '';
        if ($received_mark >= 90) {
            $grade = 'A - Excellent';
        } elseif ($received_mark >= 80) {
            $grade = 'B - Very Good';
        } elseif ($received_mark >= 70) {
            $grade = 'C - Good';
        } elseif ($received_mark >= 60) {
            $grade = 'D - Satisfactory';
        } else {
            $grade = 'F - Fail';
        }

        echo "<h2>Student Information</h2>";
        echo "<table border='1'>";
        echo "<tr><td>Student's Name:</td><td>$student_name</td></tr>";
        echo "<tr><td>Student's Surname:</td><td>$student_surname</td></tr>";
        echo "<tr><td>Course:</td><td>$course</td></tr>";
        echo "<tr><td>Semester:</td><td>$semester</td></tr>";
        echo "<tr><td>Study Course:</td><td>$study_course</td></tr>";
        echo "<tr><td>Received Mark:</td><td>$received_mark</td></tr>";
        echo "<tr><td>Corresponding Grade:</td><td>$grade</td></tr>";
        echo "<tr><td>Lecturer's Name:</td><td>$lecturer_name</td></tr>";
        echo "<tr><td>Lecturer's Surname:</td><td>$lecturer_surname</td></tr>";
        echo "</table>";
    }
    ?>
</body>
</html>
