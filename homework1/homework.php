<!DOCTYPE html>
<html>
<head>
    <title>Student Check-In Questions</title>
</head>
<body>
    <h2>Student Check-In Questions</h2>
    <form action="" method="post">
    <h3>Test Questions:</h3>
<ol>
    <li>
        <p>What is the capital of France?</p>
        <input type="radio" name="q1" value="a">a) Paris<br>
        <input type="radio" name="q1" value="b">b) London<br>
        <input type="radio" name="q1" value="c">c) Berlin<br>
        <input type="radio" name="q1" value="d">d) Madrid<br>
    </li>
    <li>
        <p>Which planet is known as the Red Planet?</p>
        <input type="radio" name="q2" value="a">a) Venus<br>
        <input type="radio" name="q2" value="b">b) Mars<br>
        <input type="radio" name="q2" value="c">c) Jupiter<br>
        <input type="radio" name="q2" value="d">d) Saturn<br>
    </li>
    <li>
        <p>What is the largest mammal on Earth?</p>
        <input type="radio" name="q3" value="a">a) Elephant<br>
        <input type="radio" name="q3" value="b">b) Giraffe<br>
        <input type="radio" name="q3" value="c">c) Blue Whale<br>
        <input type="radio" name="q3" value="d">d) Lion<br>
    </li>
</ol>


        <h3>Open Questions:</h3>
        <ol>
            <li>
                <p>Write the chemical symbol for oxygen:</p>
                <input type="text" name="q4">
            </li>
            <li>
                <p>Who is the author of "Romeo and Juliet"?</p>
                <input type="text" name="q5">
            </li>
        </ol>

        <input type="submit" name="submit" value="Submit">
    </form>

    <?php
    if (isset($_POST['submit'])) {
        $correct_answers = 0;

        if ($_POST["q1"] === 'a') {
            $correct_answers++;
        }
        if ($_POST['q2'] === 'b') {
            $correct_answers++;
        }
        if ($_POST['q3'] === 'c') {
            $correct_answers++;
        }

        $q4_answer = strtolower(trim($_POST['q4']));
        if ($q4_answer === 'oxygen') {
            $correct_answers++;
        }

        $q5_answer = strtolower(trim($_POST['q5']));
        if ($q5_answer === 'william shakespeare') {
            $correct_answers++;
        }

        echo "<h3>Number of Correct Answers: $correct_answers out of 5</h3>";
    }
    ?>
</body>
</html>
