<?php
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $email = $_POST["email"];
    $password = $_POST["password"];
    $confirm_password = $_POST["confirm_password"];
    $security_code = $_POST["security_code"];

    
    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
        echo "Error: Invalid email format.<br>";
    }

    
    if (strlen($password) < 8 || !preg_match('/\d/', $password)) {
        echo "Error: Password must be at least 8 characters long .<br>";
    }

    
    if ($password !== $confirm_password) {
        echo "Error: Passwords do not match.<br>";
    }

    
    if (strlen($security_code) !== 5) {
        echo "Error: Security code must be a 5-digit string.<br>";
    } else {
        
    }
}
?>
