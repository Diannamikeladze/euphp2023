<?php
if ($_SERVER["REQUEST_METHOD"] == "POST" && isset($_POST["file_data"])) {
    $file_data = $_POST["file_data"];
    $file_name = date("Y-m-d-H-i-s") . ".txt"; 

  
    if (file_put_contents($file_name, $file_data)) {
        echo "File '$file_name' has been created and saved successfully.";
    } else {
        echo "Error: Unable to create and save the file.";
    }
}
?>
