<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>string</title>
</head>
<body>
<?php
function isAllLowercase($str) {
   
    $lowercaseStr = strtolower($str);
   
    return $str === $lowercaseStr;
}


$inputString = "lowercase String";
if (isAllLowercase($inputString)) {
    echo "The string is  lowercase.";
} else {
    echo "The string is not  lowercase.";
}
?>

</body>
</html>