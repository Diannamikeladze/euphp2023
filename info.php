<!DOCTYPE html>
<html>
<head>
    <title> Form</title>
</head>
<body>
    <form method="post" action="action.php">
        <label for="name">სახელი :</label>
        <input type="text" name="name" id="name"><br>

        <label for="last_name">გვარი :</label>
        <input type="text" name="last_name" id="last_name"><br>

        <label for="dob">დაბადების თარიღი (Date of Birth):</label>
        <input type="date" name="dob" id="dob"><br>

        <label for="personal_number">პირადი ნომერი :</label>
        <input type="text" name="personal_number" id="personal_number"><br>

        <label for="address">მისამართი :</label>
        <input type="text" name="address" id="address"><br>

        <label for="registration_date">რეგისტრაციის თარიღი :</label>
        <input type="date" name="registration_date" id="registration_date"><br>

        <label for="mobile">მობილური :</label>
        <input type="text" name="mobile" id="mobile"><br>

        <label for="additional_info">დამატებითი ინფორმაცია :</label>
        <textarea name="additional_info" id="additional_info"></textarea><br>

        <input type="submit" value="შენახვა">
    </form>
</body>
</html>
