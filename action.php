<!DOCTYPE html>
<html>
<head>
    <title>Results</title>
</head>
<body>
    <?php
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        
        function validateNotEmpty($value, $fieldName) {
            if (empty($value)) {
                echo "<p>$fieldName ველი ცარიელია</p>";
                return false;
            }
            return true;
        }

        function validateLength($value, $fieldName, $min, $max) {
            $length = mb_strlen($value, 'UTF-8');
            if ($length < $min || $length > $max) {
                echo "<p>$fieldName უნდა იყოს $min-დან $max სიმბოლო</p>";
                return false;
            }
            return true;
        }

        function validateDate($value, $fieldName) {
            $currentYear = date('Y');
            $dobYear = date('Y', strtotime($value));
            if ($dobYear < 1950 || $dobYear > $currentYear) {
                echo "<p>$fieldName უნდა იყოს 1950-დან ზემოთ</p>";
                return false;
            }
            return true;
        }

        function validatePersonalNumber($value, $fieldName) {
            if (!preg_match('/^\d{11}$/', $value)) {
                echo "<p>$fieldName უნდა იყოს 11 ციფრი</p>";
                return false;
            }
            return true;
        }

        
        $name = $_POST['name'];
        $last_name = $_POST['last_name'];
        $dob = $_POST['dob'];
        $personal_number = $_POST['personal_number'];
        $address = $_POST['address'];
        $registration_date = $_POST['registration_date'];
        $mobile = $_POST['mobile'];
        $additional_info = $_POST['additional_info'];

        $isValid = true;

      
        $isValid = validateNotEmpty($name, 'სახელი') && $isValid;
        $isValid = validateLength($name, 'სახელი', 2, 20) && $isValid;

        $isValid = validateNotEmpty($last_name, 'გვარი') && $isValid;
        $isValid = validateLength($last_name, 'გვარი', 3, 50) && $isValid;

        $isValid = validateDate($dob, 'დაბადების თარიღი') && $isValid;
        $isValid = validatePersonalNumber($personal_number, 'პირადი ნომერი') && $isValid;

       
        if ($isValid) {
            echo "<table>";
            echo "<tr><td>სახელი:</td><td>$name</td></tr>";
            echo "<tr><td>გვარი:</td><td>$last_name</td></tr>";
            echo "<tr><td>დაბადების თარიღი:</td><td>$dob</td></tr>";
            echo "<tr><td>პირადი ნომერი:</td><td>$personal_number</td></tr>";
            echo "<tr><td>მისამართი:</td><td>$address</td></tr>";
            echo "<tr><td>რეგისტრაციის თარიღი:</td><td>$registration_date</td></tr>";
            echo "<tr><td>მობილური:</td><td>$mobile</td></tr>";
            echo "<tr><td>დამატებითი ინფორმაცია:</td><td>$additional_info</td></tr>";
            echo "</table>";
        }
    }
    ?>
</body>
</html>
